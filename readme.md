# Tabletop Simulator resources

This repository contains self-authored resources for hex-based sci-fi games in Tabletop Simulator. The resources were made for Battletech, 
but can be used for any other hex-based board game.

If you feel something is missing or could be improved, feel free to open an issue or - even better - open a PR with the improvements.